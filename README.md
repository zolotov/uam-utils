# model utils

The project re-implements and extends a set of utility programs for the Upper Atmosphere Model. The utilities are aimed at UAM model datafile processing.