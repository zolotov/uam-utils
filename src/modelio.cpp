#include "modeldatetime.h"
#include "modelio.h"

#include <assert.h>
#include <ctime>
#include <iomanip>
#include <iostream>

namespace oz::model::io {

auto open_model_datafile(const std::string &file_name) -> std::fstream {
  std::fstream f;
  f.open(file_name,
         std::ifstream::in | std::ifstream::out | std::ifstream::binary);
  return std::move(f);
}

model_info load_header(const char *path) {
  model_info mi;
  std::ifstream fin(path, std::ifstream::in | std::ifstream::binary);
  fin.seekg(0, std::ios_base::beg);
  fin.read(mi.as_char, 1024 * 4);
  fin.close();
  return std::move(mi);
}

model_info load_header(std::fstream &f) {
  model_info mi;
  f.seekg(0, std::ios_base::beg);
  f.read(mi.as_char, 1024 * 4);
  return std::move(mi);
}

bool is_model_datafile(const model_info &mi) {
  return std::string(mi.hdr.model_fingerprint) == std::string("GLOBAL MODEL");
}

auto get_model_offset(const int dataset_id, const model_info &mi) {

  const int storage_unit_id = mi._legacy_names_.NF[dataset_id - 1];
  const int storage_idx = storage_unit_id - 1;
  const int unit_position_idx =
      static_cast<int>(mi.hdr.storage_position[storage_idx]);

  int chunk_offset = -1;
  int element_id = -1;

  switch (storage_unit_id) {
  // Sphere (i.e., termosphere) datasets
  case 5:
  case 9:
  case 11:
    chunk_offset = static_cast<int>(mi._legacy_names_.NZAPS);
    element_id = static_cast<int>(mi._legacy_names_.NADRS);
    break;

  // Tube (i.e., ionosphere) datasets;
  case 6:
  case 13:
  case 12:
  case 14:
    chunk_offset = static_cast<int>(mi._legacy_names_.NZAPT);
    element_id = static_cast<int>(mi._legacy_names_.NADRT);
    break;

  // Electric potential IDs:
  case 15:
  case 4:
    chunk_offset = static_cast<int>(mi._legacy_names_.NZAPP);
    element_id = static_cast<int>(mi._legacy_names_.NADRP);
    break;

  case 1: // Magnitosphere
  case 2:
    chunk_offset = static_cast<int>(mi._legacy_names_.NZAPM);
    element_id = static_cast<int>(mi._legacy_names_.NADRM);
    break;

  case 7: // NZAPI interp. Tube -> Sphere
    chunk_offset = static_cast<int>(mi._legacy_names_.NZAPI);
    element_id = static_cast<int>(mi._legacy_names_.NADRI);
    break;

  default:
    assert(false && "Unknown dataset number. No way to proceed.");
    break;
  }

  return model_offset(unit_position_idx, chunk_offset, element_id);
}

auto get_offset_in_bytes(const int dataset_id, const model_info &mi) {

  auto mo = get_model_offset(dataset_id, mi);

  const int storage_unit_idx = mo.storage_unit_id - 1;
  const int chunk_offset = mo.chunk_offset;
  const int element_idx = mo.element_id - 1;

  const int offset_in_floats =
      (storage_unit_idx + chunk_offset) * mi.hdr.storage_chunk_size +
      element_idx;
  return offset_in_floats * sizeof(float);
};

int write_data_buf(const char *const buf, const int size_of_buf,
                   const int offset, std::fstream &f) {

  f.seekg(offset, std::ios_base::beg);

  if (!f.write(buf, size_of_buf)) {
    std::cout << "Can not write to file!!!\n";
    return 8;
  }

  if (!f.flush()) {
    std::cout << "Can not flush the data to the file!\n";
    return 9;
  }

  return 0;
}

int write_date_label(const int year, const int day_of_year,
                     const int dataset_no, const model_info &mi,
                     std::fstream &f) {
  const float buf[] = {static_cast<float>(day_of_year),
                       static_cast<float>(year)};
  const int offset_in_bytes =
      get_offset_in_bytes(dataset_no, mi) + sizeof(float) * 2;

  return write_data_buf((const char *const)buf, sizeof(buf), offset_in_bytes,
                        f);
}

int write_ut_label(const int ut, const int dataset_no, const model_info &mi,
                   std::fstream &f) {

  const float buf[] = {static_cast<float>(ut), static_cast<float>(ut)};

  const int offset_in_bytes = get_offset_in_bytes(dataset_no, mi);
  return write_data_buf((const char *const)buf, sizeof(buf), offset_in_bytes,
                        f);
}

void print_allocation_table(const model_info &mi, const char *const prefix,
                            const char *const postfix) {
  std::cout << prefix;
  for (int i = 0; i < 17; ++i) {
    std::cout << std::setw(3) << (i + 1);
  }
  std::cout << '\n';

  for (int i = 0; i < 17; ++i) {
    std::cout << std::setw(3)
              << static_cast<int>(mi.hdr.storage_allocation_id[i]);
  }
  std::cout << postfix;
  return;
};

void print_dataset_element(const int dataset_id, const model_info &mi,
                           std::fstream &fi) {

  static const char *const labels[] = {"  0 UNEXPECTED ID                  ",
                                       "  1 (NFMN) data set: Magnetosph.   ",
                                       "  2 (NFMS) data set: Magnetosph.   ",
                                       "  3 UNEXPECTED ID                  ",
                                       "  4 (NFPS) data set: Potential     ",
                                       "  5 (NFSN) data set: Termosphere   ",
                                       "  6 (NFTN) data set: Ionosphere    ",
                                       "  7 (NFIN) data set: int.ISp->TSp  ",
                                       "  8 UNEXPECTED ID                  ",
                                       "  9 (NFSS) data set: Termosphere   ",
                                       " 10 UNEXPECTED ID                  ",
                                       " 11 (NFSP) data set: Termosphere   ",
                                       " 12 (NFTP) data set: Ionosphere    ",
                                       " 13 (NFTS) data set: Ionosphere    ",
                                       " 14 (NFTR) data set: Ionosphere    ",
                                       " 15 (NFPN) data set: Potential     ",
                                       " 16 UNEXPECTED ID                  ",
                                       " 17 UNEXPECTED ID                  "};

  static const char *const monthes[] = {
      "January   ", "February  ", "March     ", "April     ",
      "May       ", "June      ", "July      ", "August    ",
      "September ", "October   ", "November  ", "December  "};

  const int storage_id = mi._legacy_names_.NF[dataset_id - 1];
  const int offset_in_bytes = get_offset_in_bytes(dataset_id, mi);

  model_time mt;
  fi.seekg(offset_in_bytes, std::ios_base::beg);
  fi.read(mt.as_char, sizeof(mt.as_char));

  const int year = static_cast<int>(mt.timelabel.GOD);
  const int day_no = static_cast<int>(mt.timelabel.DAY);

  std::tm tm;
  std::fill((char *)&tm, (char *)&tm + sizeof(tm), char(0));

  tm.tm_year = year - 1900;
  tm.tm_mday = day_no;
  tm.tm_isdst = 0;
  std::mktime(&tm);

  std::cout << labels[storage_id] << std::setw(4)
            << static_cast<int>(mt.timelabel.GOD) << " " << monthes[tm.tm_mon]
            << std::setw(2) << tm.tm_mday << " (day " << std::setw(3)
            << static_cast<int>(mt.timelabel.DAY) << ")"

            << "  UT  " << std::setw(2)
            << oz::model::time::full_hours(mt.timelabel.UT1) << ":"
            << std::setw(2) << oz::model::time::full_minutes(mt.timelabel.UT1)
            << ":" << std::setw(2)
            << oz::model::time::seconds_in_minute(mt.timelabel.UT1) << " "
            << "\n";
  return;
}

void print_version() {
  std::cout << "oz-wr version 0.1b.\n\
(C) 2019. Oleg V. Zolotov (ZolotovO@gmail.com)\n";
  return;
}

void print_help() {
  std::cout << "(C) Oleg V. Zolotov [ZolotovO@gmail.com]\n\
OZ-WR - Program for writing date- and time-labels for the model datasets.\n\
Designed as replacement for the WRTIME utility and emulates it\'s command line\n\
options and behaviour.\n\
  USAGE:\n\
          oz-wr\n\
  [-or-]  oz-wr filename\n\
  [-or-]  oz-wr [options]\n\
  WHERE\n\
    filename  - Name of the model datafile. Should not match literally \n\
                to \'version\' or \'help\' key  options\n\
     -n       - dataset number for the batch mode\n\
     -d       - date dd.mm.yyyy for the batch mode\n\
     -t       - time hh:mm:ss for the batch mode\n\
     -f       - filename for the batch mode\n\
     -v\n\
    --version - version of the executable\n\
     -h\n\
    --help\n\
     -?\n\
     /?\n\
      ?        - print this help message\n\
  EXAMPLE:\n\
     oz-wr -fmod4 -n5 -d12.12.2012 -t13:13:14\n\
	 - opens file [mod4] and for dataset [5] sets date to [12.12.2012]\n\
	 and UT-time to [13:13:14]\n\
	 ";
  return;
}

auto parse_args(const int argc, const char *const argv[])
    -> std::tuple<std::list<std::string>, std::map<char, std::string>> {
  std::map<char, std::string> arg_map;
  std::list<std::string> unexpected;

  if (argc < 1) {
    return make_tuple(std::move(unexpected), std::move(arg_map));
    ;
  };

  for (int i = 1; i < argc; ++i) {
    std::string arg{argv[i]};

    if (arg.length() < 2) {
      unexpected.push_back(std::move(arg));
      continue;
    }
    if (arg[0] != '-') {
      unexpected.push_back(std::move(arg));
      continue;
    }

    if (arg[1] == 'f') {
      arg_map.insert(std::pair<char, std::string>('f', std::move(arg)));
    } else if (arg[1] == 'n') {
      arg_map.insert(std::pair<char, std::string>('n', std::move(arg)));
    } else if (arg[1] == 'd') {
      arg_map.insert(std::pair<char, std::string>('d', std::move(arg)));
    } else if (arg[1] == 't') {
      arg_map.insert(std::pair<char, std::string>('t', std::move(arg)));
    } else {
      unexpected.push_back(std::move(arg));
      continue;
    }
  }
  return make_tuple(std::move(unexpected), std::move(arg_map));
}

bool has_args(const std::map<char, std::string> &args,
              const std::list<std::string> &unexpected) {
  if (!unexpected.empty()) {
    std::cout << "Unexpected arguments are found:\n";
    for (auto &v : unexpected) {
      std::cout << "[ " << v << " ]\n";
    }
    return false;
  }

  if (args.find('f') == args.end()) {
    std::cout << "No model datafile to proceed\n";
    return false;
  }

  if (args.find('n') == args.end()) {
    std::cout << "No model dataset is specified to proceed\n";
    return false;
  }

  if ((args.find('d') == args.end()) && (args.find('t') == args.end())) {
    std::cout << "Either date or time values are required\n";
    return false;
  }
  return true;
}

bool eval_single_arg(const char *const arg) {
  const std::string s{arg};

  if ((s == std::string("-v")) || (s == std::string("--version"))) {
    oz::model::io::print_version();
    return false;
  }

  if ((s == std::string("-h")) || (s == std::string("--help")) ||
      (s == std::string("-?")) || (s == std::string("/?")) ||
      (s == std::string("?"))) {
    oz::model::io::print_help();
    return false;
  }
  return true;
}

bool eval_args(std::map<char, std::string> &args) {

  auto fin = oz::model::io::open_model_datafile(args['f'].erase(0, 2));

  if (!fin) {
    std::cout << "Could not open the datafile [ " << args['f'] << " ]\n";
    return false;
  }

  auto header = oz::model::io::load_header(fin);
  if (!is_model_datafile(header)) {
    std::cout << " The file [ " << args['f'] << " ] is not model datafile.\n";
    return false;
  }

  int dataset_no;
  if (!std::sscanf(args['n'].erase(0, 2).c_str(), "%d", &dataset_no)) {
    std::cout << "Invalid dataset value: " << args['n'] << '\n';
    return false;
  }

  if ((dataset_no < 1) || (dataset_no > 17)) {
    std::cout << "Invalid dataset value: " << args['n'] << '\n';
    return false;
  }

  // date
  if (args.find('d') != args.end()) {
    int yyyy, mon, dd;
    if (std::sscanf(args['d'].erase(0, 2).c_str(), "%d.%d.%d", &dd, &mon,
                    &yyyy) != 3) {
      std::cout << "Can not parse date [ " << args['d'] << " ]\n";
      return false;
    }

    if (!oz::model::date::date_is_ok(yyyy, mon, dd)) {
      std::cout << "Incorrect datelabel!\n";
      return false;
    }

    const int day_of_year = oz::model::date::date_to_day_of_year(yyyy, mon, dd);
    if (oz::model::io::write_date_label(yyyy, day_of_year, dataset_no, header,
                                        fin)) {
      std::cout << "Error writing date label\n";
      return false;
    }
  }

  // time
  if (args.find('t') != args.end()) {
    int hh, mm, ss;
    if (std::sscanf(args['t'].erase(0, 2).c_str(), "%d:%d:%d", &hh, &mm, &ss) !=
        3) {
      std::cout << "Can not parse time [ " << args['t'] << " ]\n";
      return false;
    }

    if (!oz::model::time::time_is_ok(hh, mm, ss)) {
      std::cout << "Incorrect timelabel!\n";
      return false;
    }

    if (oz::model::io::write_ut_label(
            oz::model::time::ut_to_seconds(hh, mm, ss), dataset_no, header,
            fin)) {
      std::cout << "Error on writing the UT label\n";
      return false;
    }
  }
  fin.close();
  return true;
}

} // namespace oz::model::io
