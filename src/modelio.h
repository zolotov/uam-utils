#ifndef _modelio_h_
#define _modelio_h_

#include "model_types.h"
#include <fstream>
#include <list>
#include <map>

namespace oz::model::io {

auto open_model_datafile(const std::string &file_name) -> std::fstream;

model_info load_header(const char *path);
model_info load_header(std::fstream &f);

int write_date_label(const int year, const int day_of_year,
                     const int dataset_no, const model_info &mi,
                     std::fstream &f);
int write_ut_label(const int ut, const int dataset_no, const model_info &mi,
                   std::fstream &f);

void print_allocation_table(
    const model_info &mi,
    const char *const prefix =
        " Allocation of data sets in the disk file regions\n",
    const char *const postfix = "\n\n");

void print_dataset_element(const int dataset_id, const model_info &mi,
                           std::fstream &fi);

void print_version();
void print_help();

auto parse_args(const int argc, const char *const argv[])
    -> std::tuple<std::list<std::string>, std::map<char, std::string>>;

bool has_args(const std::map<char, std::string> &args,
              const std::list<std::string> &unexpected);

bool eval_single_arg(const char *const arg);
bool eval_args(std::map<char, std::string> &args);

bool is_model_datafile(const model_info &mi);

} // namespace oz::model::io

#endif
