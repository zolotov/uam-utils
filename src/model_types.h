#ifndef _model_types_h_
#define _model_types_h_

// Holds fields of the informational record (i.e. parsed array of 1024 floats)
typedef struct {
  float storage_allocation_id[17]; // NF(17)
  char description[80];
  float storage_position[20];                   // KDF[20]
  float storage_size[20];                       // KDU[20];
  float tube_species_count;                     // KPART
  float sphere_species_count;                   // KPARS
  float sphere2tube_interpolated_species_count; // INT
  float tube2sphere_interpolated_species_count; // INS
  float storage_chunk_size;                     // MDOR
  float storage_chunk_count;                    // NDF
  float longitudinal_step;                      // DDOLGT
  float _unused_85_;
  float _unused_86_;
  float _unused_87_;
  float sphere_min_altitude;      // RMIN
  float sphere_min_altitude_step; // DH
  float sphere_altitude_levels;   // NH
  float altitude_step_factor;     // GAMMA
  float NTR;
  float upper_boundary_altitude; // RMAXT
  float _unused_94_;
  float _unused_95_;
  float tube_timelabel_chunk_id;      // NZAPT
  float sphere_timelabel_chunk_id;    // NZAPS
  float tube_timelabel_id;            // NADRT
  float sphere_timelabel_id;          // NADRS
  float tubes_count;                  // NL
  float tube_planes_count;            // NTPL*2
  float tube_size_of_PARI;            // NTPL*INT; s -> t interpolated values
  float tube_size_of_PAR;             // NTPL*KPART;
  float potential_timelabel_chunk_id; // NZAPP
  float potential_timelabel_id;       // NADRP
  float sphere_colatitudes_count;     // ITS
  float interpolated_t2s_timelabel_chunk_id; // NZAPI
  float interpolated_t2s_timelabel_id;       // NADRI
  float _unused_109_;
  float _unused_110_;
  float tube_nodes_count[90]; // NTSL[90];
  float _not_used_in_current_version_CONTIN[100];
  float potential_colatitudes[100]; // TETMT(NL2): Stores only (NL2-3)/2 nodes,
                                    // i.e. only the half of the TETMT array
  float sphere_colatitudes[100]; // TETMS
  float sphere_altitudes[30];    // RADS(NH)
  float _unused_531_[490];
  char model_fingerprint[12];
  float model_version;
} model_header;

// The same as above but to use the OLD NAMES of variables and paramenters
typedef struct {
  float NF[17]; // NF(17)
  char description[80];
  float KDF[20];
  float KDU[20];
  float KPART;
  float KPARS;
  float INT;
  float INS;
  float MDOR;
  float NDF;
  float DDOLGT;
  float _unused_85_;
  float NHDown; // _unused_86_;
  float _unused_87_;
  float RMIN;
  float DH;
  float NH;
  float GAMMA;
  float NTR;
  float RMAXT;
  float _unused_94_;
  float _unused_95_;
  float NZAPT;
  float NZAPS;
  float NADRT;
  float NADRS;
  float NL;    // 100
  float NTPL;  // NTPL*2 - tube_size_of_PARK
  float NZAPM; // tube_size_of_PARI; // NTPL*INT; s -> t interpolated values
  float NADRM; // tube_size_of_PAR;  // NTPL*KPART;
  float NZAPP;
  float NADRP;
  float ITS;
  float NZAPI;
  float NADRI;
  float _unused_109_;
  float _unused_110_;
  float NTSL[90];
  float _not_used_CONTIN[100];
  float TETMT[100]; // TETMT(NL2): Stores only (NL2-3)/2 nodes, i.e. only the
                    // half of the TETMT array
  float TETMS[100]; // TETMS
  float RADS[30];   // RADS(NH)
  float _unused_531_[490];
  char model_fingerprint[12];
  float model_version;
} model_legacy_names_header;

// Holds the model info-record. Used for typecasting between fixed-length array
// of floats vs fields of a structure (to avoid manual offset/indexes
// calculation and corresponding boilerplate functions manual writing).
typedef union {
  static_assert((sizeof(char) == 1), "Unexpected char size!");
  static_assert((sizeof(float) == 4), "Unexpected float size!");
  static_assert((sizeof(model_header) == sizeof(model_legacy_names_header)),
                "Incompatible header vs legacy header struct!");

  char as_char[1024 * 4];
  float as_float[1024];
  model_header hdr;
  model_legacy_names_header _legacy_names_;

  static_assert((sizeof(as_char) == sizeof(as_float)) &&
                    (sizeof(as_char) == sizeof(hdr)) &&
                    (sizeof(as_char) == sizeof(_legacy_names_) &&
                    (sizeof(as_char) == 1024 * 4)),
                "Incompatible representation of THE SAME bytestream!");
} model_info;

// Hold the model time-label
typedef struct {
  float UT0;
  float UT1;
  float DAY;
  float GOD;
  float FA;
  float PKP;
} model_time_label;

// To give names for the elements of the array
// and to simplify loading and storing of the values
typedef union {
  char as_char[6 * sizeof(float)];
  float as_float[6];
  model_time_label timelabel;

  static_assert(sizeof(as_char) == sizeof(as_float),
                "Invalid structure size!!!");
  static_assert(sizeof(as_char) == sizeof(timelabel),
                "Invalid structure size!!!");
} model_time;

// To simplify addressing inside binary file structures
struct model_offset {
  int storage_unit_id;
  int chunk_offset;
  int element_id;

  model_offset(int _storage_unit_id, int _chunk_offset, int _element_id)
      : storage_unit_id(_storage_unit_id), chunk_offset(_chunk_offset),
        element_id(_element_id){};
};

#endif