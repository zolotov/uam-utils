#ifndef _modeldatetime_h_
#define _modeldatetime_h_

#include "model_types.h"

constexpr get_dataset_id(const int _id, const model_info &mi) {
  for (int i = 0; i < sizeof(mi.hdr.storage_allocation_id); ++i) {
    const int ds_id = static_cast<int>(mi.hdr.storage_allocation_id[i]);
    if (_id == ds_id) {
      return (i + 1);
    }
  }
  return 0;
}

namespace oz::model::time {

constexpr bool time_is_ok(const int hh, const int mm, const int ss) {
  return (hh >= 0) && (hh <= 24) && (mm >= 0) && (mm < 60) && (ss >= 0) &&
         (ss < 60);
}

constexpr int full_hours(const int seconds) { return seconds / 3600; }

constexpr int full_minutes(const int seconds) {
  return (seconds - full_hours(seconds) * 3600) / 60;
}

constexpr int seconds_in_minute(const int seconds) {
  return seconds - full_hours(seconds) * 3600 - full_minutes(seconds) * 60;
}
constexpr int ut_to_seconds(const int hh, const int mm, const int ss) {
  return hh * 3600 + mm * 60 + ss;
}

} // namespace oz::model::time

namespace oz::model::date {

constexpr int is_leap_year(const int year) {
  return ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) ? 1 : 0;
}

constexpr bool date_is_ok(const int yyyy, const int mon, const int dd) {
  return (yyyy >= 0) && (mon > 0) && (mon <= 12) && (dd >= 0) && (dd <= 31);
}

constexpr int date_to_day_of_year(int year, int month, int day) {
  const int days_count[2][13] = {
   //{ 0, 31, 28, 31,  30,  31,  30,  31,  31,  30,  31,  30,  31},
      {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365},

   //{ 0, 31, 29, 31,  30,  31,  30,  31,  31,  30,  31,  30,  31}
      {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366}};
  return days_count[is_leap_year(year)][month - 1] + day;
}
} // namespace oz::model::date

#endif