#include "interact.h"
#include "model_types.h"
#include "modeldatetime.h"
#include "modelio.h"
#include <iostream>

int main(int argc, char *argv[]) {

  switch (argc) {
  case 1: // No input arguments
    return oz::model::interact::interact_with_user(nullptr);

  case 2: // exactly one argument - treat it as model datafile vs. 'help' or
          // 'version' options;
    if (!oz::model::io::eval_single_arg(argv[1])) {
      return 0;
    }
    return oz::model::interact::interact_with_user(argv[1]);

  case 4: // exactly three or four arguments
  case 5: {  
    auto [un, ar] = oz::model::io::parse_args(argc, argv);
    if (!oz::model::io::has_args(ar, un)) {
		
      return 1;
    }
    return !oz::model::io::eval_args(ar);
  }
  default: // unknown options
    return 11;
  }
  //return 0;
}