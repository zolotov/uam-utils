#ifndef _interact_h_
#define _interact_h_

#include "model_types.h"

#include <string>
#include <tuple>

namespace oz::model::interact {

auto ask_for_model_datafile_name() -> std::string;
auto ask_for_model_dataset_number() -> std::tuple<bool, int>;
auto ask_for_dataset_datetime()
    -> std::tuple<bool, int, int, int, int, int, int>;

int interact_with_user(const char *const argv);

} // namespace oz::model::interact

#endif