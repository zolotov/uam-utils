#include "interact.h"
#include "modeldatetime.h"
#include "modelio.h"

#include <iostream>

namespace oz::model::interact {

auto ask_for_model_datafile_name() -> std::string {
  std::cout << "Input file name:\n";
  std::string f_name;
  std::cin >> f_name;
  return std::move(f_name);
}

auto ask_for_model_dataset_number() -> std::tuple<bool, int> {
  std::cout << "Input top number of the data set for writitng the time label\n"
            << "0 - return to DOS\n";
  int dataset_no;
  std::cin >> dataset_no;
  std::cin.ignore(2, '\n'); // pop EOL
  if (!std::cin.good()) {
    std::cout << "Invalid interger value!\n";
    return std::make_tuple(false, dataset_no);
  }
  return std::make_tuple(true, dataset_no);
}

auto ask_for_dataset_datetime()
    -> std::tuple<bool, int, int, int, int, int, int> {
  std::cout << "Input time for writing into the data set label /dd.mm.yyyy "
               "hh:mm:ss/:\n";
  std::string to_parse;
  std::getline(std::cin, to_parse, '\n');

  int yyyy = -1, mon = -1, dd = -1, hh = 0, mm = 0, ss = 0;
  std::sscanf(to_parse.c_str(), "%d.%d.%d %d:%d:%d", &dd, &mon, &yyyy, &hh, &mm,
              &ss);

  if (yyyy < 0) {
    std::cout << "The date-time entered is not valid.\n";
    return std::make_tuple(false, 0, 0, 0, 0, 0, 0);
  }
  return std::make_tuple(true, yyyy, mon, dd, hh, mm, ss);
}

int interact_with_user(std::fstream &f, const model_info &mh) {

  oz::model::io::print_allocation_table(mh);

  auto [ret_code, dataset_no] =
      oz::model::interact::ask_for_model_dataset_number();

  if (!ret_code) {
    return 3;
  }

  if (!dataset_no) { // return to DOS
    return 0;
  }

  if ((dataset_no < 0) || (dataset_no > 17)) {
    std::cout << "Data set number is out of range [1..17]!\n";
    return 4;
  }

  if (!mh._legacy_names_.NF[dataset_no - 1]) {
    std::cout << "No storage space is allocated for the dataset No "
              << dataset_no << '\n';
    return 5;
  }

  oz::model::io::print_dataset_element(dataset_no, mh, f);

  auto [ret_code1, yyyy, mon, dd, hh, mm, ss] =
      oz::model::interact::ask_for_dataset_datetime();
  if (!ret_code1) {
    return 6;
  }

  const bool date_is_set = (yyyy >= 0) && (mon >= 0) && (dd >= 0);
  if (date_is_set) { // Zeroes are kept for the backward compatability with
                     // WRTIME.
    if (mon > 12) {  // To prevent index-out-of-range exploit...
      std::cout << "The month value is incorrect. You entered " << mon << '\n';
      return 7;
    }
    std::cout << "Setting the date\n";
    const int day_of_year = oz::model::date::date_to_day_of_year(yyyy, mon, dd);
    oz::model::io::write_date_label(yyyy, day_of_year, dataset_no, mh, f);
  }

  const bool time_is_set = (hh >= 0) && (mm >= 0) && (ss >= 0);
  if (time_is_set) {
    oz::model::io::write_ut_label(oz::model::time::ut_to_seconds(hh, mm, ss),
                                  dataset_no, mh, f);
  }

  return -1;
}

int interact_with_user(const char *const argv) {

  std::string arg{(argv) ? std::string{argv}
                         : oz::model::interact::ask_for_model_datafile_name()};

  std::fstream f = oz::model::io::open_model_datafile(arg);
  if (!f.good()) {
    std::cout << "Could not open file " << '\n';
    return 1;
  }

  auto model_header = oz::model::io::load_header(f);
  if (!oz::model::io::is_model_datafile(model_header)) {
    std::cout << "The file is not model datafile!\n";
    return 2;
  }

  // processing
  int code;
  while ((code = interact_with_user(f, model_header)) < 0) {
  }

  return code;
}

} // namespace oz::model::interact
